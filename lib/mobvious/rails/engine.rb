module Mobvious
module Rails
  # Definition of this class makes this gem a Rails engine, enabling
  # providing assets that can be used in the application.
  class Engine < ::Rails::Engine
    initializer 'mobvious-rails.assets' do |config|
      # I've commented this out so that coffee-script isn't required, which breaks my stack with an MRI crash
      #::Rails.application.config.assets.precompile.unshift 'mobvious-rails.js'
    end
  end
end
end
